<?php

namespace App\RMVC\Route;

class RouteDispatcher {

    private $requestURI = '/';

    private $paramMap = [];

    private $routeConfiguration;

    public function __construct(RouteConfiguration $routeConfiguration) {
        $this->routeConfiguration = $routeConfiguration;
    }

    public function proccess() {

        $this->saveRequestUri();

        $this->setParamMap();
    }
 
    private function saveRequestUri() {

        if ($_SERVER['REQUEST_URI'] !== '/') {
            $this->requestURI = $this->clean($_SERVER['REQUEST_URI']);
            $this->routeConfiguration->route = $this->clean($this->routeConfiguration->route);
        }


        // echo '<pre>';
        // var_dump($this->requestURI);
        // var_dump($this->routeConfiguration->route);
        // echo '</pre>';
    

    }

    private function clean($string) {
        return preg_replace('/(^\/)|(\/$)/', '' , $string);
    }


    private function setParamMap() {
        $routeArray = explode('/', $this->routeConfiguration->route);
    
        echo '<pre>';
        var_dump($routeArray);
        var_dump($this->routeConfiguration->route);
        echo '</pre>';
    
    }

}