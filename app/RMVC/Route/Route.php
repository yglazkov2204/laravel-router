<?php

namespace App\RMVC\Route;

class Route {

    private static $routesGet = [];

    public static function get($route, $controller) {
        $routeConfiguration = new RouteConfiguration($route, $controller[0], $controller[1]);

        self::$routesGet[] = $routeConfiguration;

        return $routeConfiguration;
    }

    public static function getRoutesGet() {
        return self::$routesGet;
    }


}