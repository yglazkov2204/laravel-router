<?php

namespace App\RMVC\Route;

class RouteConfiguration {

    public $route;
    public $controller;
    public $action;
    private $name;
    private $middleware;
    public function __construct($route, $controller, $action) {
        $this->route = $route;
        $this->controller = $controller;
        $this->action = $action;
    }

    public function name($name) {
        $this->name = $name;
        return $this;
    }

    public function middleware($middleware) {
        $this->middleware = $middleware;
        return $this;
    }

}