<?php

use App\RMVC\Route\Route;


Route::get('/posts', [PostController::class, 'index'])->name('post.index')->middleware('user.auth');